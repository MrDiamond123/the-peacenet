﻿using System;
namespace Plex.Engine.Discord
{
    public enum Reply
    {
        No = 0,
        Yes = 1,
        Ignore = 2
    }
}