using Microsoft.Xna.Framework.Graphics;
using NAudio.Wave;

namespace Plex.Engine.Cutscenes
{
    public struct VideoFrame
    {
        public Texture2D picture;
        public WaveStream sound;
    }
}

