﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plex.Objects;

namespace Plex.Server
{
    public static class Terminal
    {
        private static string _shelloverride = "";

        public static string SessionID { get; private set; }

        public static void SetShellOverride(string value)
        {
            _shelloverride = value;
        }

        public static List<TerminalCommand> Commands { get; private set; }

        public static void Populate()
        {
            Commands = new List<TerminalCommand>();
            foreach (var type in ReflectMan.Types)
            {
                foreach (var mth in type.GetMethods(BindingFlags.Public | BindingFlags.Static))
                {

                    var cmd = mth.GetCustomAttributes(false).FirstOrDefault(x => x is ServerCommand) as ServerCommand;
                    if (cmd != null)
                    {
                        var tc = new TerminalCommand();
                        tc.RequiresElevation = false;

                        var shellConstraint = mth.GetCustomAttributes(false).FirstOrDefault(x => x is ShellConstraintAttribute) as ShellConstraintAttribute;
                        tc.ShellMatch = (shellConstraint == null) ? "" : shellConstraint.Shell;

                        if (mth.GetCustomAttributes(false).FirstOrDefault(x => x is MetaCommandAttribute) != null)
                        {
                            tc.ShellMatch = "metacmd";
                        }

                        tc.CommandInfo = cmd;
                        tc.RequiresElevation = false;
                        tc.RequiredArguments = new List<string>();
                        //foreach (var arg in mth.GetCustomAttributes(false).Where(x => x is RequiresArgument))
                        //{
                        //    var rarg = arg as RequiresArgument;
                        //    tc.RequiredArguments.Add(rarg.Name);
                        //} Obsolete.

                        foreach (var attr in mth.GetCustomAttributes(false).Where(x => x is UsageStringAttribute))
                        {
                            tc.UsageStrings.Add(attr as UsageStringAttribute);
                        }


                        var rupg = mth.GetCustomAttributes(false).FirstOrDefault(x => x is RequiresUpgradeAttribute) as RequiresUpgradeAttribute;
                        if (rupg != null)
                            tc.Dependencies = rupg.Upgrade;
                        else
                            tc.Dependencies = "";
                        tc.CommandType = type;
                        tc.CommandHandler = mth;

                        var ambiguity = Commands.FirstOrDefault(x => x.CommandInfo.name == tc.CommandInfo.name);
                        if (ambiguity != null)
                            throw new Exception("Command ambiguity error. You can't have two commands with the same name: " + $"{tc} == {ambiguity}");

                        if (!Commands.Contains(tc))
                            Commands.Add(tc);
                    }
                }

            }
            Console.WriteLine("[termdb] " + Commands.Count + " commands found.");
        }

        [ServerMessageHandler(ServerMessageType.TRM_MANPAGE)]
        [SessionRequired]
        public static void GetManPage(string session_id, BinaryReader reader, BinaryWriter writer)
        {
            string cmdname = reader.ReadString();

            var cmd = Commands.FirstOrDefault(x => x.CommandInfo.name == cmdname && UpgradeManager.IsUpgradeLoaded(x.Dependencies, session_id));
        }


        public static bool RunClient(string text, string[] args, string session_id, StreamWriter stdout, StreamReader stdin, bool isServerAdmin = false)
        {
            SessionID = session_id;
            var cmd = Commands.FirstOrDefault(x => x.CommandInfo.name == text);
            if (cmd == null)
                return false;
            if(((ServerCommand)cmd.CommandInfo).ServerOnly == true && isServerAdmin == false)
            {
                stdout.WriteLine("You can't run this command as you are not a server admin.");
                return true;
            }
            if (!UpgradeManager.IsUpgradeLoaded(cmd.Dependencies, session_id))
                return false;
            try
            {
                cmd.Invoke(args, _shelloverride, stdout, stdin);
            }
            catch (TargetInvocationException ex)
            {
                stdout.WriteLine("Command error: " + ex.InnerException.Message);
            }

            return true;
        }

        [ServerCommand("echo", "Prints the desired text on-screen.")]
        [UsageString("<text>")]
        public static void Echo(Dictionary<string, object> args)
        {
            Console.WriteLine(args["<text>"].ToString());
        }

        public static RequestInfo SessionInfo { get; private set; }

        [ServerMessageHandler( ServerMessageType.TRM_GETCMDS)]
        [SessionRequired]
        public static byte GetHelp(string session_id, BinaryReader reader, BinaryWriter writer)
        {
            Dictionary<string, string> commands = new Dictionary<string, string>();
            foreach (var cmd in Terminal.Commands)
            {
                if (UpgradeManager.IsUpgradeLoaded(cmd.Dependencies, session_id))
                    commands.Add(cmd.CommandInfo.name, cmd.CommandInfo.description);
            }
            writer.Write(commands.Count);
            foreach(var cmd in commands)
            {
                writer.Write(cmd.Key);
                writer.Write(cmd.Value);
            }
            return 0x00;
        }


        [ServerMessageHandler( ServerMessageType.TRM_INVOKE)]
        [SessionRequired]
        public static byte InvokeCMD(string session_id, BinaryReader reader, BinaryWriter writer)
        {
            //TODO: Persistent connection to client so that stdin reads from client and stdout sends to client.
            //This will allow server-side commands to ask the in-game user for input while the command is running.
            //For now, we'll pipe stdout and stdin to a MemoryStream.
            MemoryStream std = new MemoryStream();
            var stdout = new StreamWriter(std);
            stdout.AutoFlush = true;
            var stdin = new StreamReader(std);

            string datajson = reader.ReadString();
            SessionInfo = new RequestInfo
            {
                SessionID = session_id
            };
            var outstream = Console.Out;
            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(datajson);
            SetShellOverride(data["shell"].ToString());
            string sessionfwd = (string.IsNullOrWhiteSpace(data["sessionfwd"] as string)) ? session_id : data["sessionfwd"].ToString();
            bool result = RunClient(data["cmd"].ToString(), JsonConvert.DeserializeObject<string[]>(JsonConvert.SerializeObject(data["args"])), sessionfwd, stdout, stdin);
            SetShellOverride("");
            writer.Write(Encoding.UTF8.GetString(std.ToArray()));
            stdout.Close();
            stdin.Close();
            std.Close();
            writer.Write("\u0013\u0014");
            return 0x00;
        }
    }

    public class RequestInfo
    {
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public string SessionID { get; set; }
    }

    public class RemoteTextWriter : System.IO.TextWriter
    {
        public override Encoding Encoding
        {
            get
            {
                return Encoding.UTF8;
            }
        }

        private BinaryWriter _writer = null;
        private string _session = "";

        public RemoteTextWriter(BinaryWriter writer, string session)
        {
            _writer = writer;
            _session = session;
        }

        public override void Write(string value)
        {
            _writer.Write(value);
        }

        public override void WriteLine(string value)
        {
            Write(value + Environment.NewLine);
        }

    }
}
