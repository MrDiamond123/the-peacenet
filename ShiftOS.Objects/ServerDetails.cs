﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plex.Objects
{
    public class ServerDetails
    {
        public string Hostname { get; set; }
        public int Port { get; set; }
        public string FriendlyName { get; set; }
    }
}
